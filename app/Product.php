<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = [''];

    /**
     * Product belongs to category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Product has many comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get photos
     *
     * @param $value
     * @return mixed
     */
    public function getPhotosAttribute($value)
    {
        return json_decode($value);
    }
}
