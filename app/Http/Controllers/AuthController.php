<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Set the site title in constructor.
     *
     */
    function __construct()
    {
        $this->site_title = 'Sloppy2ndz';
    }

    /**
     * View the login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        $data = [];
        $data['site_title'] = $this->site_title;
        $data['page_title'] = 'Login';
        return view('login', $data);
    }

    /**
     * Process the login request.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function processLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            return redirect()->route('dashboard');
        }

        session()->flash('message', 'Invalid login credentials.');
        return redirect()->back()->withInput();
    }

    /**
     * Logs out the user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();

        // Logs out the user...
        session()->flash('message', 'You have been logged out.');
        return redirect()->route('login');
    }
}
