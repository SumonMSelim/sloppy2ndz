<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{
    /**
     * Register a new user.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'username' => 'required|min:6|unique:users',
            'password' => 'required|min:6',
            'gender' => 'required',
            'country' => 'required',
            'profile_image' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $path = public_path('uploads/profile');
        $image_file = $request->profile_image;
        $file_name = str_random(16) . '-img.' . $image_file->getClientOriginalExtension();

        Image::make($image_file)
            ->save($path . '/' . $file_name)
            ->resize(150, 150)->save($path . '/thumb/' . $file_name);

        $array = [
            'first_name' => trim($request->first_name),
            'last_name' => trim($request->last_name),
            'email' => trim($request->email),
            'username' => trim($request->username),
            'password' => bcrypt(trim($request->password)),
            'gender' => trim($request->gender),
            'country' => trim($request->country),
            'profile_image' => trim(url('uploads/profile/thumb/' . $file_name)),
        ];

        $user = User::create($array);
        $data['status'] = 1;
        $data['msg'] = 'registration success';
        $data['results'] = User::find($user->id);

        return response()->json($data);
    }

    /**
     * Login an user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'identifier' => 'required|min:6',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed, if identifier is username, get the email
        if (filter_var($request->identifier, FILTER_VALIDATE_EMAIL) == false) {
            $user = User::where('username', $request->identifier)->first();
            if ($user) $email = $user->email;
        } else {
            $user = User::where('email', $request->identifier)->first();
            if ($user) $email = $request->identifier;
        }

        if (!isset($email)) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = 'username/email is not registered';

            return response()->json($data);
        }

        $credentials = [
            'email' => trim($email),
            'password' => trim($request->password),
        ];

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                $data['status'] = 0;
                $data['msg'] = 'validation errors';
                $data['errors'] = 'invalid username/email or password';

                return response()->json($data);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = 'could not create token';

            return response()->json($data);
        }

        // all good so return the token
        $data['status'] = 1;
        $data['msg'] = 'login success';
        $data['results'] = $user;
        $data['token'] = $token;

        return response()->json($data);
    }

    /**
     * Login an user via Facebook
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginViaFacebook(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'facebook' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed, check if user with the facebook id exists
        $user = User::where('facebook', $request->facebook)->first();

        if (!$user) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = 'user with the provided facebook id does not exist';

            return response()->json($data);
        }

        try {
            $token = JWTAuth::fromUser($user);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = 'could not create token';

            return response()->json($data);
        }

        // all good so return the token
        $data['status'] = 1;
        $data['msg'] = 'login success';
        $data['results'] = $user;
        $data['token'] = $token;

        return response()->json($data);
    }

    /**
     * Login an user via Instagram
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginViaInstagram(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'instagram' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed, check if user with the instagram id exists
        $user = User::where('instagram', $request->instagram)->first();

        if (!$user) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = 'user with the provided instagram id does not exist';

            return response()->json($data);
        }

        try {
            $token = JWTAuth::fromUser($user);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = 'could not create token';

            return response()->json($data);
        }

        // all good so return the token
        $data['status'] = 1;
        $data['msg'] = 'login success';
        $data['results'] = $user;
        $data['token'] = $token;

        return response()->json($data);
    }

    /**
     * Edit profile.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editProfile(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'username' => 'required|min:6',
            'gender' => 'required',
            'country' => 'required',
            'short_bio' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'first_name' => trim($request->first_name),
            'last_name' => trim($request->last_name),
            'email' => trim($request->email),
            'username' => trim($request->username),
            'gender' => trim($request->gender),
            'country' => trim($request->country),
            'short_bio' => trim($request->short_bio),
        ];

        if (!empty($request->passwword)) {
            $array['password'] = bcrypt(trim($request->password));
        }

        if (!empty($request->profile_image)) {
            $path = public_path('uploads/profile');
            $image_file = $request->profile_image;
            $file_name = str_random(16) . '-img.' . $image_file->getClientOriginalExtension();

            Image::make($image_file)
                ->save($path . '/' . $file_name)
                ->resize(150, 150)->save($path . '/thumb/' . $file_name);

            $array['profile_image'] = trim(url('uploads/profile/thumb/' . $file_name));
        }

        User::find($user->id)
            ->update($array);

        $data['status'] = 1;
        $data['msg'] = 'edit profile success';
        $data['results'] = User::find($user->id);

        return response()->json($data);
    }

    /**
     * Edit billing information.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editBilling(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($request->all(), [
            'billing_type' => 'required',
            'billing_name' => 'required',
            'billing_address' => 'required',
            'billing_city' => 'required',
            'billing_state' => 'required',
            'billing_zip' => 'required',
            'billing_country' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'billing_type' => trim($request->billing_type),
            'billing_name' => trim($request->billing_name),
            'billing_address' => trim($request->billing_address),
            'billing_city' => trim($request->billing_city),
            'billing_state' => trim($request->billing_state),
            'billing_zip' => trim($request->billing_zip),
            'billing_country' => trim($request->billing_country),
        ];

        User::find($user->id)
            ->update($array);

        $data['status'] = 1;
        $data['msg'] = 'edit billing success';
        $data['results'] = User::select([
            'id',
            'username',
            'billing_type',
            'billing_name',
            'billing_address',
            'billing_city',
            'billing_state',
            'billing_zip',
            'billing_country',
        ])->find($user->id);

        return response()->json($data);
    }

    /**
     * Edit Paypal.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPaypal(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($request->all(), [
            'paypal_email' => 'required|email',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'paypal_email' => trim($request->paypal_email),
        ];

        User::find($user->id)
            ->update($array);

        $data['status'] = 1;
        $data['msg'] = 'edit paypal success';
        $data['results'] = User::select([
            'id',
            'username',
            'paypal_email',
        ])->find($user->id);

        return response()->json($data);
    }

    /**
     * Edit bank.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editBank(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($request->all(), [
            'bank_account' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'bank_account' => trim($request->bank_account),
        ];

        User::find($user->id)
            ->update($array);

        $data['status'] = 1;
        $data['msg'] = 'edit bank account success';
        $data['results'] = User::select([
            'id',
            'username',
            'bank_account',
        ])->find($user->id);

        return response()->json($data);
    }

    /**
     * Edit currency.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editCurrency(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($request->all(), [
            'currency' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'currency' => trim($request->currency),
        ];

        User::find($user->id)
            ->update($array);

        $data['status'] = 1;
        $data['msg'] = 'edit currency success';
        $data['results'] = User::select([
            'id',
            'username',
            'currency',
        ])->find($user->id);

        return response()->json($data);
    }

    /**
     * Profile of the autheticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        $data = [];
        $data['code'] = 200;
        $data['status'] = 1;
        $data['msg'] = 'profile of autheticated user';
        $data['results'] = JWTAuth::parseToken()->toUser();

        return response()->json($data);
    }

    /**
     * Add a product category.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCategory(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'name' => trim($request->name),
        ];

        // if description is provided
        if (!empty($request->description)) {
            $array['description'] = trim($request->description);
        }

        // if image is provided
        if (!empty($request->image)) {
            $path = public_path('uploads/category');
            $image_file = $request->image;
            $file_name = str_random(16) . '-img.' . $image_file->getClientOriginalExtension();

            Image::make($image_file)
                ->save($path . '/' . $file_name)
                ->resize(150, 150)->save($path . '/thumb/' . $file_name);

            $array['image'] = trim(url('uploads/category/thumb/' . $file_name));
        }

        $category = Category::create($array);
        $data['status'] = 1;
        $data['msg'] = 'category add success';
        $data['results'] = Category::find($category->id);

        return response()->json($data);
    }

    /**
     * Show all categories
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listCategories()
    {
        $data = [];
        $data['code'] = 200;

        $categories = Category::all();

        if ($categories->count() > 0) {
            $data['status'] = 0;
            $data['msg'] = 'no category found';
            $data['results'] = 'no category found';

            return response()->json($data);
        }

        $data['status'] = 1;
        $data['msg'] = 'category list success';
        $data['results'] = $categories;

        return response()->json($data);
    }

    /**
     * Show category details by id.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param $id
     */
    public function categoryDetails(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'categoryId' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        $data['status'] = 1;
        $data['msg'] = 'product details success';
        $data['results'] = Category::find($request->categoryId);

        return response()->json($data);
    }

    /**
     * Add or sell a product.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProduct(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'photos' => 'required',
            'category_id' => 'required|numeric',
            'brand' => 'required',
            'size' => 'required',
            'original_price' => 'required',
            'listing_price' => 'required',
            'hashtags' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'user_id' => JWTAuth::parseToken()->toUser()->id,
            'name' => trim($request->name),
            'description' => trim($request->name),
            'category_id' => trim($request->category_id),
            'brand' => trim($request->brand),
            'size' => trim($request->size),
            'original_price' => (float)trim($request->original_price),
            'listing_price' => (float)trim($request->listing_price),
            'hashtags' => trim($request->hashtags),
        ];

        if (!empty($request->new_tag)) $array['new_tag'] = trim($request->new_tag);
        if (!empty($request->delivery_meet_in_person)) $array['delivery_meet_in_person'] = trim($request->delivery_meet_in_person);
        if (!empty($request->delivery_shipping)) $array['delivery_shipping'] = trim($request->delivery_shipping);
        if (!empty($request->delivery_international_shipping)) $array['delivery_international_shipping'] = trim($request->delivery_international_shipping);
        if (!empty($request->status)) $array['status'] = trim($request->status);

        // if photos is provided
        if (!empty($request->photos)) {
            $path = public_path('uploads/product');
            $photos = [];

            if (is_array($request->photos)) {
                foreach ($request->photos as $key => $photo) {
                    $image_file = $photo;
                    $file_name = str_random(16) . '-img.' . $image_file->getClientOriginalExtension();

                    Image::make($image_file)
                        ->save($path . '/' . $file_name)
                        ->resize(150, 150)->save($path . '/thumb/' . $file_name);

                    $num = $key + 1;
                    $photos["photo_$num"] = trim(url('uploads/product/thumb/' . $file_name));
                }
            } else {
                $image_file = $request->photos;
                $file_name = str_random(16) . '-img.' . $image_file->getClientOriginalExtension();

                Image::make($image_file)
                    ->save($path . '/' . $file_name)
                    ->resize(150, 150)->save($path . '/thumb/' . $file_name);

                $photos["photo_1"] = trim(url('uploads/product/thumb/' . $file_name));
            }
            $array['photos'] = json_encode($photos);
        }

        $product = Product::create($array);
        $data['status'] = 1;
        $data['msg'] = 'product add success';
        $data['results'] = Product::find($product->id);

        return response()->json($data);
    }

    /**
     * Show product details by id.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param $id
     */
    public function productDetails(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'productId' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        $data['status'] = 1;
        $data['msg'] = 'product details success';
        $data['results'] = Product::find($request->productId);

        return response()->json($data);
    }

    /**
     * Add product comment.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addComment(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|numeric',
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        // validation passed
        $array = [
            'product_id' => (int)trim($request->product_id),
            'comment' => trim($request->comment),
        ];

        // if user_id is provided
        if (!empty($request->user_id)) {
            $array['user_id'] = trim($request->user_id);
        } else {
            $array['user_id'] = JWTAuth::parseToken()->toUser()->id;
        }

        $comment = Comment::create($array);
        $data['status'] = 1;
        $data['msg'] = 'comment add success';
        $data['results'] = Comment::find($comment->id);

        return response()->json($data);
    }

    /**
     * List comments of a product.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param $id
     */
    public function listComments(Request $request)
    {
        $data = [];
        $data['code'] = 200;

        $validator = Validator::make($request->all(), [
            'productId' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $data['status'] = 0;
            $data['msg'] = 'validation errors';
            $data['errors'] = $validator->errors()->first();

            return response()->json($data);
        }

        $data['status'] = 1;
        $data['msg'] = 'product comments success';
        $data['results'] = Comment::where('product_id',$request->productId)->get();

        return response()->json($data);
    }
}
