@extends('layouts.master')
@section('main')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"> Products
                <small>products list</small>
            </h3>

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Products</span>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover order-column">
                        <thead>
                        <tr>
                            <th> Name</th>
                            <th> Brand</th>
                            <th> Original Price</th>
                            <th> Listing Price</th>
                            <th> Status</th>
                            <th> Added By</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $produt)
                            <tr class="odd gradeX">
                                <td>{{ $produt->name }}</td>
                                <td>{{ $produt->brand }}</td>
                                <td>{{ $produt->original_price  }}</td>
                                <td>{{ $produt->listing_price }}</td>
                                <td>{{ $produt->status }}</td>
                                <td>{{ $produt->user->first_name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!!  $products->render() !!}
                </div>
            </div>

        </div>
    </div>
@endsection