@extends('layouts.master')
@section('main')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"> Users
                <small>users list</small>
            </h3>

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Users</span>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover order-column">
                        <thead>
                        <tr>
                            <th> Username</th>
                            <th> Email</th>
                            <th> Profile Image</th>
                            <th> Country</th>
                            <th> Joined</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr class="odd gradeX">
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td><img src="{{ $user->profile_image  }}"></td>
                                <td>{{ $user->country }}</td>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!!  $users->render() !!}
                </div>
            </div>

        </div>
    </div>
@endsection