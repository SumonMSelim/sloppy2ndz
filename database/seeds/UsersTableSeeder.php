<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Sloppy',
            'last_name' => '2ndz',
            'username' => 'sloppy2ndz',
            'email' => 'sloppy2ndz@aapbd.com',
            'password' => bcrypt('aapbd.com'),
            'gender' => 'male',
            'role' => 'admin',
            'country' => 'Bangladesh',
        ]);
    }
}
